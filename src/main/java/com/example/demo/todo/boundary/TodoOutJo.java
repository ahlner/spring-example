package com.example.demo.todo.boundary;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

@Relation(value = "todo", collectionRelation = "todos")
public class TodoOutJo extends ResourceSupport {

    private String note;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        TodoOutJo todoOutJo = (TodoOutJo) o;
        return Objects.equal(note, todoOutJo.note);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(super.hashCode(), note);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("note", note)
                .toString();
    }
}
