package com.example.demo.configuration;

import org.springframework.boot.test.web.client.LocalHostUriTemplateHandler;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class TestConfiguration {


   @Bean
    public RestTemplate testRestTemplate(RestTemplateBuilder restTemplateBuilder, ApplicationContext applicationContext) {
        LocalHostUriTemplateHandler handler = new LocalHostUriTemplateHandler(applicationContext.getEnvironment());
        final RestTemplate template = restTemplateBuilder.build();
        template.setUriTemplateHandler(handler);

        return template;
    }
}
