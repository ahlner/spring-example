package com.example.demo.wiring;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.atomic.AtomicBoolean;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WiringTest {

    @Autowired
    @Qualifier("bool1")
    private AtomicBoolean atomicBoolean;

    @Test
    public void contextLoads() {

    }


    @Configuration
    public static class MyTestConfiguration {

        @Bean
        public AtomicBoolean bool2(AtomicBoolean bool1) {
            return new AtomicBoolean(bool1.get());
        }

        @Bean
        public AtomicBoolean bool1() {
            return new AtomicBoolean();
        }





    }



}
