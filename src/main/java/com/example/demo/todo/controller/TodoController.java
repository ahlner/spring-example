package com.example.demo.todo.controller;

import com.example.demo.exceptions.NotFoundException;
import com.example.demo.todo.entity.Todo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class TodoController {

    private final TodoRepository todoRepository;

    public TodoController(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public Optional<Todo> findByResourceKey(String resourceKey) {
        return todoRepository.findByResourceKey(resourceKey);
    }

    public Page<Todo> findAll(Pageable pageable) {
        return todoRepository.findAll(pageable);
    }

    public Todo create(String note) {
        return todoRepository.save(new Todo(note));
    }

    public void delete(String resourceKey) throws NotFoundException {
        final Todo todo = findByResourceKey(resourceKey).orElseThrow(() -> new NotFoundException());
        todoRepository.delete(todo);
    }

    public Todo save(Todo todo) {
        return todoRepository.save(todo);
    }
}
