package com.example.demo.todo.boundary;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import javax.validation.constraints.NotEmpty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TodoInJo {

    @NotEmpty
    private String note;

    public TodoInJo() {
    }

    public TodoInJo(@NotEmpty String note) {
        this.note = note;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TodoInJo todoInJo = (TodoInJo) o;
        return Objects.equal(note, todoInJo.note);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(note);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("note", note)
                .toString();
    }
}
