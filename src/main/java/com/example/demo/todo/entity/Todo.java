package com.example.demo.todo.entity;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(indexes = @Index(name = "idx_todo_resource_key", columnList = "resource_key"))
@EntityListeners(AuditingEntityListener.class)
public class Todo {

    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty
    @Column(name = "resource_key")
    private String resourceKey = UUID.randomUUID().toString();

    @Column(name = "created_on", nullable = false)
    @CreatedDate
    private Date createdOn;

    @Column(name = "changed_on", nullable = false)
    @LastModifiedDate
    private Date changedOn;

    private String note;

    public Todo() {
    }

    public Todo(String note) {
        this.note = note;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getResourceKey() {
        return resourceKey;
    }

    public void setResourceKey(String resourceKey) {
        this.resourceKey = resourceKey;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getChangedOn() {
        return changedOn;
    }

    public void setChangedOn(Date changedOn) {
        this.changedOn = changedOn;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Todo todo = (Todo) o;
        return Objects.equal(id, todo.id) &&
                Objects.equal(resourceKey, todo.resourceKey) &&
                Objects.equal(createdOn, todo.createdOn) &&
                Objects.equal(changedOn, todo.changedOn) &&
                Objects.equal(note, todo.note);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, resourceKey, createdOn, changedOn, note);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("resourceKey", resourceKey)
                .add("createdOn", createdOn)
                .add("changedOn", changedOn)
                .add("note", note)
                .toString();
    }
}
