package com.example.demo.todo.boundary;

import com.example.demo.exceptions.NotFoundException;
import com.example.demo.todo.controller.TodoController;
import com.example.demo.todo.entity.Todo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping(path = "/todos", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaTypes.HAL_JSON_UTF8_VALUE})
public class TodoRestController {

    private final TodoResourceAssembler todoResourceAssembler;

    private final TodoController todoController;

    private final PagedResourcesAssembler<Todo> pagedResourcesAssembler;


    public TodoRestController(TodoResourceAssembler todoResourceAssembler, TodoController todoController, PagedResourcesAssembler<Todo> pagedResourcesAssembler) {
        this.todoResourceAssembler = todoResourceAssembler;
        this.todoController = todoController;
        this.pagedResourcesAssembler = pagedResourcesAssembler;
    }

    @GetMapping(path = "/{resourceKey}")
    public TodoOutJo get(@PathVariable String resourceKey) throws NotFoundException {
        final Todo todo = todoController.findByResourceKey(resourceKey).orElseThrow(() -> new NotFoundException());

        return todoResourceAssembler.toResource(todo);
    }

    @PostMapping
    public ResponseEntity<TodoOutJo> post(@RequestBody @Valid TodoInJo todoInJo) {
        final Todo todo = todoController.create(todoInJo.getNote());

        final TodoOutJo todoOutJo = todoResourceAssembler.toResource(todo);
        final String href = todoOutJo.getLink(Link.REL_SELF).getHref();
        final ResponseEntity<TodoOutJo> responseEntity = ResponseEntity.created(URI.create(href)).body(todoOutJo);

        return responseEntity;

        //return HeaderLinksResponseEntity.wrap(responseEntity);
    }

    @GetMapping
    public PagedResources<TodoOutJo> getAll(@PageableDefault Pageable pageable) {

        final Page<Todo> todoPage = todoController.findAll(pageable);
        final PagedResources<TodoOutJo> outJos = pagedResourcesAssembler.toResource(todoPage, todoResourceAssembler);

        return outJos;
    }

    @PutMapping(path = "/{resourceKey}")
    public TodoOutJo update(@PathVariable String resourceKey, @RequestBody @Valid TodoInJo body) throws NotFoundException {
        final Todo todo = todoController.findByResourceKey(resourceKey).orElseThrow(() -> new NotFoundException());

        todo.setNote(body.getNote());

        return todoResourceAssembler.toResource(todoController.save(todo));
    }

    @DeleteMapping(path = "/{resourceKey}")
    public void delete(@PathVariable String resourceKey) throws NotFoundException {
        todoController.delete(resourceKey);
    }


}

