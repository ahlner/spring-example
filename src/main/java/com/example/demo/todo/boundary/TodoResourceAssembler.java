package com.example.demo.todo.boundary;

import com.example.demo.todo.entity.Todo;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class TodoResourceAssembler extends ResourceAssemblerSupport<Todo, TodoOutJo> {

    public TodoResourceAssembler() {
        super(TodoRestController.class, TodoOutJo.class);
    }

    @Override
    public TodoOutJo toResource(Todo entity) {
        TodoOutJo result = createResourceWithId(entity.getResourceKey(), entity);
        result.setNote(entity.getNote());
        return result;
    }
}
