package com.example.demo.todo.boundary;

import com.example.demo.exceptions.NotFoundException;
import com.example.demo.todo.controller.TodoRepository;
import com.example.demo.todo.entity.Todo;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.mvc.TypeConstrainedMappingJackson2HttpMessageConverter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TodoRestControllerTest {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private TodoRepository todoRepository;


    private TypeConstrainedMappingJackson2HttpMessageConverter halJacksonHttpMessageConverter;


    @Before
    public void setUp() {
        cleanUp();
        todoRepository.save(new Todo("note1"));
        todoRepository.save(new Todo("note2"));
        todoRepository.save(new Todo("note3"));
        assertThat(todoRepository.count()).isEqualTo(3);
    }

    @After
    public void cleanUp() {
        todoRepository.deleteAll();
    }

    @Test
    public void create() {
        cleanUp();

        final String noteX = "noteX";
        final ResponseEntity<Void> response = restTemplate.postForEntity("/todos", new TodoInJo(noteX), Void.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getHeaders()).containsKey(HttpHeaders.LOCATION);

        assertThat(todoRepository.findAll()).hasSize(1).extracting(Todo::getNote).containsExactly(noteX);
    }

    @Test
    public void get() {
        final Todo todo = todoRepository.findAll().get(0);
        String resourceKey = todo.getResourceKey();

        final ResponseEntity<TodoOutJo> responseEntity = restTemplate.getForEntity("/todos/{resourceKey}", TodoOutJo.class, resourceKey);

        assertThat(responseEntity.getStatusCode().is2xxSuccessful()).isTrue();
        assertThat(responseEntity.getBody().getNote()).isEqualTo(todo.getNote());
    }

    @Test
    public void update() throws NotFoundException {
        String resourceKey = todoRepository.findAll().get(0).getResourceKey();

        final String noteX = "noteX";
        restTemplate.put("/todos/{resourceKey}", new TodoInJo(noteX), resourceKey);

        final Todo todo = todoRepository.findByResourceKey(resourceKey).orElseThrow(() -> new NotFoundException());
        assertThat(todo.getNote()).isEqualTo(noteX);
    }

    @Test
    public void delete() {
        String resourceKey = todoRepository.findAll().get(0).getResourceKey();

        restTemplate.delete("/todos/{resourceKey}", resourceKey);

        assertThat(todoRepository.count()).isEqualTo(2);
    }

    @Test
    public void getAll() {
        final ResponseEntity<PagedResources<TodoOutJo>> responseEntity = restTemplate.exchange("/todos", HttpMethod.GET, null, new ParameterizedTypeReference<PagedResources<TodoOutJo>>() {
        });

        assertThat(responseEntity.getStatusCode().is2xxSuccessful()).isTrue();
        assertThat(responseEntity.getBody()).extracting(TodoOutJo::getNote).containsExactlyInAnyOrder("note1", "note2", "note3");
    }
}